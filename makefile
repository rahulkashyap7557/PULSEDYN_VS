#
# Makefile for PULSEDYN. To build the program, use make all or simply make.
# To delete the object files for a fresh build, use make clean.
# The make commands must be typed in the terminal.
#
# Define C++ compiler. Default is g++
#
# -std=c++11      sets the c++ 11 standard. 
# -O3            sets the -O3 level of compiler optimization
# -fopt-info-vec-optimized     shows successful vectorization
CC=g++
CFLAGS= -Wall -fexceptions -O3 -std=c++11 -fopt-info-vec-optimized
RM=rm -f
RMD=rm -r 

# default option is make all. However, even just make works.
all: bin/PULSEDYN

bin/PULSEDYN: main.o obj/boundaryConditions.o obj/force.o obj/fpuPotential.o \
		  obj/lennardJonesPotential.o obj/morsePotential.o obj/Output.o \
		  obj/Particle.o obj/Simulation.o obj/todaPotential.o
	- mkdir bin 
	- $(CC) -o bin/PULSEDYN obj/boundaryConditions.o obj/force.o obj/fpuPotential.o \
		  obj/lennardJonesPotential.o obj/morsePotential.o obj/Output.o \
		  obj/Particle.o obj/Simulation.o obj/todaPotential.o main.o $(CFLAGS)
	- chmod ugo+x bin/PULSEDYN

main.o: main.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h	
	- $(CC) -c -o main.o -I include main.cpp $(CFLAGS)
	
obj/boundaryConditions.o: src/boundaryConditions.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- mkdir obj
	- $(CC) -c -o obj/boundaryConditions.o -I include src/boundaryConditions.cpp $(CFLAGS)
	
obj/force.o: src/force.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/force.o -I include src/force.cpp $(CFLAGS)
	
obj/fpuPotential.o: src/fpuPotential.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h	
	- $(CC) -c -o obj/fpuPotential.o -I include src/fpuPotential.cpp $(CFLAGS)

	
obj/lennardJonesPotential.o: src/lennardJonesPotential.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/lennardJonesPotential.o -I include src/lennardJonesPotential.cpp $(CFLAGS)


obj/morsePotential.o: src/morsePotential.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/morsePotential.o -I include src/morsePotential.cpp $(CFLAGS)


obj/Output.o: src/Output.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/Output.o -I include src/Output.cpp $(CFLAGS)
		
obj/Particle.o: src/Particle.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/Particle.o -I include src/Particle.cpp $(CFLAGS)
		
obj/Simulation.o: src/Simulation.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/Simulation.o -I include src/Simulation.cpp $(CFLAGS)
		
obj/todaPotential.o: src/todaPotential.cpp include/allIncludes.h include/boundaryConditions.h \
		include/force.h include/fpuPotential.h include/lennardJonesPotential.h \
		include/morsePotential.h include/Output.h include/Particle.h \
		include/Simulation.h include/todaPotential.h
	- $(CC) -c -o obj/todaPotential.o -I include src/todaPotential.cpp $(CFLAGS)
#
# Use make clean to clear just the object files and executables.
# Use make clean-all to delete the bin and obj folders with all their contents 
# Finally make clean and make clean-dir will remove any *~ backup files
#
clean:
	- $(RM) bin/PULSEDYN
	- $(RM) obj/*.o obj/*~
	- $(RM) main.o
#
clean-dir:
	- $(RMD) bin
	- $(RMD) obj
	-$(RM) main.o