/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

//#include "include/Simulation.h"
//#include "include/System.h"
//#include "include/Particle.h"
#include "include/allIncludes.h"
#include "include/Logger.h"
#include "include/ChainParticles.h"
//#include "include/fpuPotential.h"
using namespace std;

int main()
{

    /*
    TODO: Rahul Kashyap 10/20/2019
    1. Move the parser to a new function/class (done)
    2. Make the integration methods store their own parameters. Add a few more methods. The method used in the CUDA paper looks good.
    3. Each method must have the steps written into them separately. Don't hard code this in Simulation.h
    Simulation.h must just be a junction from where the flow gets routed into the correct method
    4. Add cases for external perturbation of any kind at any given time point (although this is technically not necessary unless the perturbation is continuous)
    For this, extend the external ExternalPerturbation method to allow perturbations of x and v as well. Allow boundaries to change halfway through the simulation is necessary.
    5. Energy calculation methods must be written up separately for the entire chain.
    6. Consider adding work done if there is an external force.
    7. See if perhaps the vectorization can be done more efficiently.
    8. Simplify the operations.
    9. Add feature to store individual spring parameters and type so that the force can be calculated regardless of spring combos.
    10. Extend this to store building blocks of potentials and forces so that a combination can be applied to each spring and force can
    be calculated.
    11. Only specified files should be copied. Build a dictionary and query which files need to be written.
    12. Allow for custom file names.
    13. Turn all implementations into pImpl.
    14. If the force and external perturbations are generalized, make sure dissipation is handled correctly.
    */

    // Set up a timer to start a clock and estimate running times

    // Record start time
    auto start = std::chrono::high_resolution_clock::now();

    // Initialize the initial conditions parser
    std::shared_ptr<InitialConditionsParser> parser = InitialConditionsParser::Create();
    bool parsingSuccessful = parser->ParseParameterFile();



    Simulation simParams;
    
    std::shared_ptr<ChainParticles> chainParticles = ChainParticles::Create();
    std::shared_ptr<ExternalPerturbationsManager> externalPerturbationManager;

    // TODO: Why should parser set the variables?
    // Why aren't variables set inside simParams by passing in the parser?
    parser->SetVariables(simParams, chainParticles);
    externalPerturbationManager = parser->GetExternalPerturbationsManager();

    // Check if files with the same names as the output from this simulation exist. If they do delete them.
    // Check if position file exists and delete if it does.    
    parser->CheckAndDeleteExistingOutputFiles();
    
    cout << "Setting up simulation" << endl;

    // Create all kinds of Spring Objects
    std::vector<fpuPotential> fpuObject(1);
    std::vector<todaPotential> todaObject(1);
    std::vector<morsePotential> morseObject(1);
    std::vector<lennardJonesPotential> lennardJonesObject(1);
    std::vector<CoulombPotential> coulombObject(1);

    std::string springType = parser->GetSpringType();
    
    double k1 = parser->GetK1();
    double k2 = parser->GetK2();
    double k3 = parser->GetK3();

    // Check which model is chosen and accordingly start simulation
    // 05/22/2022 RKP: This needs to be done much better. Some polymorphism needed here
    if (springType == "toda")
    {
        todaObject[0].Setk1(k1);
        todaObject[0].Setk2(k2);
        simParams.f_startSim(chainParticles, todaObject, externalPerturbationManager);
    }
    else if (springType == "fput")
    {
        fpuObject[0].Setk1(k1);
        fpuObject[0].Setk2(k2);
        fpuObject[0].Setk3(k3);
        simParams.f_startSim(chainParticles, fpuObject, externalPerturbationManager);
    }
    else if (springType == "morse")
    {
        morseObject[0].Setk1(k1);
        morseObject[0].Setk2(k2);
        simParams.f_startSim(chainParticles, morseObject, externalPerturbationManager);
    }
    else if (springType == "lennardjones")
    {
        lennardJonesObject[0].Setk1(k1);
        lennardJonesObject[0].Setk2(k2);
        simParams.f_startSim(chainParticles, lennardJonesObject, externalPerturbationManager);
    }
    else if (springType == "coulomb")
    {
        coulombObject[0].Setk1(k1);
        coulombObject[0].Setk2(k2);
        coulombObject[0].Setk3(k3);
        simParams.f_startSim(chainParticles, coulombObject, externalPerturbationManager);
    }
    else
    {
        cout << "No recognized model type specified. Exiting program." << endl;
        return 0;
    }

    // Get current time after code completion
    auto finish = std::chrono::high_resolution_clock::now();

    // Calculate elapsed time
    std::chrono::duration<double> elapsed = finish - start;
    std::cout << "Time elapsed: " << elapsed.count() << "s" << endl;

    // Write the time taken into the log-file
    std::shared_ptr<Logger> logger = Logger::InitializeOrGetLoggerSingleton();

    std::string messageToWrite = "Time elapsed: " + std::to_string(elapsed.count()) + " s\n";
    logger->WriteToLog(messageToWrite);

    return 0;
}
