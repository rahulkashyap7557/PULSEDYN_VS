#pragma once
#include "../include/ExternalPerturbationsVelocityDamping.h"
#include "../include/ChainParticles.h"
#include <iostream>

ExternalPerturbationsVelocityDamping::ExternalPerturbationsVelocityDamping(
	std::string iType, 
	std::string iExternalPerturbationVariable, 
	std::string iParticleNumber, 
	std::vector<double> iParameters, 
	unsigned int iSystemSize)
{
	SetParticlesToApplyPerturbationOn(iParticleNumber, iSystemSize);
	SetParametersForExternalPerturbation(iType, iParameters);
	SetExternalPerturbationVariable(iExternalPerturbationVariable);
}

void ExternalPerturbationsVelocityDamping::vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
	std::shared_ptr<ChainParticles> ioChainParticles, 
	double iCurrentTime, 
	std::vector<double>& oPredictedAcceleration)
{
	unsigned int systemSize;
	ioChainParticles->GetSystemSize(systemSize);

	std::vector<double> velocity;
	ioChainParticles->GetVelocity(velocity);
	for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
	{
		oPredictedAcceleration[particleNumber] = -1. * this->mGamma * velocity[particleNumber];
	}
}

void ExternalPerturbationsVelocityDamping::vSetParametersForExternalPerturbation(
	std::string iType, 
	std::vector<double> iParameters)
{
	// We don't user iType since we only have velocity damping 
	// We also only expect the damping factor gamma as a parameter so check for the size of the vector
	if (iParameters.size() > 1)
	{
		std::cout << "Too many parameters for velocity damping term. Please check your parameter file." << std::endl;
		std::terminate();
	}
	else
	{
		this->mGamma = iParameters[0];
	}

}
