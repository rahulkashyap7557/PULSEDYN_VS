#include "../include/ExternalPerturbationsSawtooth.h"
#include "../include/ChainParticles.h"
#include <iostream>
#include <cmath>

ExternalPerturbationsSawtooth::ExternalPerturbationsSawtooth(
	std::string iType, 
	std::string iExternalPerturbationVariable, 
	std::string iParticleNumber, 
	std::vector<double> iParameters, 
	unsigned int iSystemSize)
{
	SetParticlesToApplyPerturbationOn(iParticleNumber, iSystemSize);
	SetExternalPerturbationVariable(iExternalPerturbationVariable);
	SetParametersForExternalPerturbation(iType, iParameters);
}

void ExternalPerturbationsSawtooth::CalculateSlopeOfSawToothWave()
{
	this->mSlopeOfSawToothWave = (this->mWaveEndValue - this->mWaveStartValue) / (this->mWidth);
}

void ExternalPerturbationsSawtooth::CalculateSawToothWavePerturbation(
    double& oPerturbationValue,
    const double& iCurrentTime)
{
    int numCyclesElapsed = floor((iCurrentTime - this->mStartTime) / this->mWidth);

    double timeElapsedInCurrentCycle = iCurrentTime - (double)numCyclesElapsed * this->mWidth;

    oPerturbationValue = this->mSlopeOfSawToothWave * timeElapsedInCurrentCycle;
}

void ExternalPerturbationsSawtooth::vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
    std::shared_ptr<ChainParticles> ioChainParticles, 
    double iCurrentTime, 
    std::vector<double>& oPredictedAcceleration)
{
    unsigned int systemSize;
    ioChainParticles->GetSystemSize(systemSize);

    if (this->mExternalPerturbationVariable == "force")
    {
//#pragma omp parallel num_threads(8)
        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateSawToothWavePerturbation(perturbation, iCurrentTime);
                oPredictedAcceleration[particleNumber] = perturbation;
            }
            // else do nothing
        }
    }
    else if (this->mExternalPerturbationVariable == "pos")
    {
        std::vector<double> positionPerturbation(systemSize, 0.);

        std::vector<double> positionValuesInChain;
        ioChainParticles->GetPosition(positionValuesInChain);

        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateSawToothWavePerturbation(perturbation, iCurrentTime);
                positionPerturbation[particleNumber] = perturbation;
                positionValuesInChain[particleNumber] += positionPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetPosition(positionValuesInChain);
        }
    }
    else if (this->mExternalPerturbationVariable == "vel")
    {
        std::vector<double> velocityPerturbation(systemSize, 0.);

        std::vector<double> velocityValuesInChain;
        ioChainParticles->GetVelocity(velocityValuesInChain);

        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateSawToothWavePerturbation(perturbation, iCurrentTime);
                velocityPerturbation[particleNumber] = perturbation;
                velocityValuesInChain[particleNumber] += velocityPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetVelocity(velocityValuesInChain);
        }
    }
    // else do nothing
}

void ExternalPerturbationsSawtooth::vSetParametersForExternalPerturbation(
	std::string iType, 
	std::vector<double> iParameters)
{
	// e, f, h, ii
	// e, idx 0 -> perturbation minimum
	// f, idx 1 -> perturbation maximum
	// g, idx 2  -> t1 (start time)
	// h, idx 3  -> t2 (end time)
	// i, idx 4 -> t3 (width of sawtooth wave)
	try
	{
		this->mPerturbationFunction = iType;

		this->mWaveStartValue = iParameters[0];
		this->mWaveEndValue = iParameters[1];
		
		this->mStartTime = iParameters[2];
		this->mEndTime = iParameters[3];

		if (this->mStartTime > this->mEndTime)
		{
			std::cout << "Start time of perturation cannot be more than end time of saw tooth wave perturbation" << std::endl;
			std::terminate();
		}

		this->mWidth = iParameters[4];
		if (this->mEndTime - this->mStartTime < this->mTolerance)
		{
			std::cout << "Start time and end time are too close together for sawtooth wave. Min difference is 1e-6. Please check your input file" << std::endl;
			std::terminate();
		}
		if (this->mWidth < this->mTolerance)
		{
			std::cout << "Width of the sawtooth wave is too small. Min 1e-6 or sawtooth wave.Please check your input file" << std::endl;
			std::terminate();
		}

		CalculateSlopeOfSawToothWave();
	}
	catch (std::exception& e)
	{
		std::cout << "Something went wrong with setting external perturbation parameters for sawtooth wave. Please check your input file" << std::endl;
		std::cout << e.what() << std::endl;

		std::terminate();
	}
}
