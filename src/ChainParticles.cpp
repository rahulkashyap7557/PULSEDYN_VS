#include "../include/ChainParticles.h"

std::shared_ptr<ChainParticles> ChainParticles::Create()
{
	return std::shared_ptr<ChainParticles>(new ChainParticles());
}

ChainParticles::ChainParticles()
{
    mSystemSize = 100;
    mLBoundary = "fixed";
    mRBoundary = "fixed";
}

void ChainParticles::SetSystemSize(unsigned int& iSystemSize)
{
    mSystemSize = iSystemSize;
	mMassVector = std::vector<double>(iSystemSize, 1.0);
	mPositionVector = std::vector<double>(iSystemSize, 0.0);
	mVelocityVector = std::vector<double>(iSystemSize, 0.0);
	mAccelVector = std::vector<double>(iSystemSize, 0.0);
	mKeVector = std::vector<double>(iSystemSize, 0.0);
}

void ChainParticles::SetLBoundary(std::string& iLBoundary)
{
	mLBoundary = iLBoundary;
}

void ChainParticles::SetRBoundary(std::string& iRBoundary)
{
	mRBoundary = iRBoundary;
}

void ChainParticles::SetInitialPerturbations(std::vector<unsigned int>& iPerturbedParticles,
    std::vector<double> iPerturbationAmplitude,
    std::vector<std::string>& iPerturbationType)
{
    double amp;
    unsigned int w;

    if (iPerturbedParticles.size() > 0)
    {
        for (int i = 0; i < iPerturbedParticles.size(); i++)
        {
            w = iPerturbedParticles.at(i);
            amp = iPerturbationAmplitude.at(i);
            if (w - 1 < mSystemSize)
            {
                if (iPerturbationType.at(i) == "vel")
                {
                    mVelocityVector[w - 1] = amp;
                    //oChainParticles[w - 1].Addvelocity(amp);
                }
                if (iPerturbationType.at(i) == "pos")
                {
                    mPositionVector[w - 1] = amp;
                    //oChainParticles[w - 1].Addposition(amp);
                }
                if (iPerturbationType.at(i) == "acc")
                {
                    mAccelVector[w - 1] = amp;
                    //oChainParticles[w - 1].Addaccel(amp);
                }
            }
        }
    }
}

void ChainParticles::SetImpurities(std::vector<unsigned int>& iImpurityParticles, std::vector<double>& iImpurityValue)
{
    double amp;
    unsigned int w;
    if (iImpurityParticles.size() > 0)
    {
        for (int i = 0; i < iImpurityParticles.size(); i++)
        {
            w = iImpurityParticles.at(i);
            amp = iImpurityValue.at(i);
            int j;
            if (w == 0)
            {
                for (j = 0; j < mSystemSize; j++)
                {
                    if (amp > 0.00000000001)
                    {
                        mMassVector[j] = amp;
                    }
                    
                    //oChainParticles[j].Setmass(amp);
                }
            }
            else
            {
                j = w - 1;
                if (amp > 0.00000000001)
                {
                    mMassVector[j] = amp;
                }                
                //oChainParticles[j].Setmass(amp);
            }
        }
    }

    //for (int i = 0; i < mSystemSize; i++)
    //{
    //    amp = oChainParticles.at(i).Getmass();
    //    if (amp <= 0.00000000001)
    //    {
    //        oChainParticles[i].Setmass(defaultMass);
    //        cout << "Mass at %d th particle was less than or equal to 0." << endl;
    //    }
    //}

}

void ChainParticles::SetPosition(std::vector<double>& iPOsition)
{
    mPositionVector = iPOsition;
}

void ChainParticles::SetVelocity(std::vector<double>& iVelocity)
{
    mVelocityVector = iVelocity;
}

void ChainParticles::SetAccel(std::vector<double>& iAccel)
{
    mAccelVector = iAccel;
}

void ChainParticles::CalculateKE()
{
    for (int ii = 0; ii < mSystemSize; ii++)
    {
        mKeVector[ii] = 0.5 * mMassVector[ii] * mVelocityVector[ii] * mVelocityVector[ii];
    }
}

void ChainParticles::GetPosition(std::vector<double>& oPosition)
{
    oPosition = mPositionVector;
}

void ChainParticles::GetVelocity(std::vector<double>& oVelocity)
{
    oVelocity = mVelocityVector;
}

void ChainParticles::GetAccel(std::vector<double>& oAccel)
{
    oAccel = mAccelVector;
}

void ChainParticles::GetMass(std::vector<double>& oMass)
{
    oMass = mMassVector;
}

void ChainParticles::GetKe(std::vector<double>& oKe)
{
    oKe = mKeVector;
}

void ChainParticles::GetTotalKineticEnergy(double& oTotalKineticEnergy)
{
    oTotalKineticEnergy = 0.;
    unsigned int systemSize = mSystemSize;
#pragma omp simd reduction(+:oTotalKineticEnergy)
    for (int ii = 0; ii < systemSize; ii++)
    {
        oTotalKineticEnergy += mKeVector[ii];
    }
}

void ChainParticles::GetSystemSize(unsigned int& oSystemSize)
{
    oSystemSize = mSystemSize;
}

void ChainParticles::GetLeftBoundary(std::string& oLeftBoundary)
{
    oLeftBoundary = mLBoundary;
}

void ChainParticles::GetRightBoundary(std::string& oRightBoundary)
{
    oRightBoundary = mRBoundary;
}
