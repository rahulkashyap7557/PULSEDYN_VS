/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once
#include "../include/ExternalPerturbationSinusoidal.h"
#include "../include/ChainParticles.h"
#include <vector>

double pi = std::atan(1) * 4;

ExternalPerturbationSinusoidal::ExternalPerturbationSinusoidal()
{
    //ctor
    perturbationFunction = "cosine";
    perturbationVariable = "position";
    t1 = 0.;
    t2 = 0.;
    t3 = 0.;
    frequency = 0.;
    ramp = 0.;
    amp = 0.;

    
}

ExternalPerturbationSinusoidal::ExternalPerturbationSinusoidal(
    std::string iType, 
    std::string iExternalPerturbationVariable,
    std::string iParticleNumber, 
    std::vector<double> iParameters,
    unsigned int iSystemSize)
{
    ExternalPerturbationSinusoidal();
    
    SetParticlesToApplyPerturbationOn(iParticleNumber, iSystemSize);
    SetExternalPerturbationVariable(iExternalPerturbationVariable);
    SetParametersForExternalPerturbation(iType, iParameters);
}

// RKP 10/26/2019: Change this to make the external perturbation calculation more streamlined.
double ExternalPerturbationSinusoidal::f_forceCalc(double tCurrent)
{
    double perturbation = 0.;
    if (perturbationFunction == "cosine")
    {
        perturbation = f_cosine(tCurrent);
    }
    else if(perturbationFunction == "sine")
    {
        perturbation = f_sine(tCurrent);
    }

    else if(perturbationFunction == "constant")
    {
        perturbation = f_constant(tCurrent);
    }
    return perturbation;
}

double ExternalPerturbationSinusoidal::f_cosine(double tCurrent)
{
    double perturbation;
    double freq1;
    freq1 = frequency + ramp*tCurrent;
    perturbation = amp*cos(freq1*tCurrent);
    return perturbation;
}

double ExternalPerturbationSinusoidal::f_sine(double tCurrent)
{
    double perturbation;
    double freq1;
    freq1 = frequency + ramp*tCurrent;
    perturbation = amp*sin(freq1*tCurrent);
    return perturbation;
}

double ExternalPerturbationSinusoidal::f_constant(double tCurrent)
{
    double perturbation = amp + ramp*tCurrent;
    return perturbation;
}

//void ExternalPerturbationSinusoidal::vApplyPerturbation(std::shared_ptr<ChainParticles> ioChainParticles)
//{
//    get rid of this function
//}

void ExternalPerturbationSinusoidal::vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
    std::shared_ptr<ChainParticles> ioChainParticles, 
    double iCurrentTime, 
    std::vector<double>& oPredictedAcceleration)
{
    //fill in external perturbation code here from simulation.h here
    unsigned int systemSize;
    ioChainParticles->GetSystemSize(systemSize);
    //oPredictedAcceleration = std::vector<double>(systemSize, 0.);

    if (this->mExternalPerturbationVariable == "force")
    {
        //for (unsigned int ii = 0; ii < systemSize; ii++)
        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > t1 && iCurrentTime < t3)
            {
                oPredictedAcceleration[particleNumber] = this->f_forceCalc(iCurrentTime);
            }
            // else do nothing
        }
    }
    else if (this->mExternalPerturbationVariable == "pos")
    {
        std::vector<double> positionPerturbation(systemSize, 0.);

        std::vector<double> positionValuesInChain;
        ioChainParticles->GetPosition(positionValuesInChain);
        //for (unsigned int ii = 0; ii < systemSize; ii++)
        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > t1 && iCurrentTime < t3)
            {
                positionPerturbation[particleNumber] = this->f_forceCalc(iCurrentTime);
                positionValuesInChain[particleNumber] += positionPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetPosition(positionValuesInChain);
        }
    }
    else if (this->mExternalPerturbationVariable == "vel")
    {
        std::vector<double> velocityPerturbation(systemSize, 0.);

        std::vector<double> velocityValuesInChain;
        ioChainParticles->GetVelocity(velocityValuesInChain);
        //for (unsigned int ii = 0; ii < systemSize; ii++)
        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > t1 && iCurrentTime < t3)
            {                
                velocityPerturbation[particleNumber] = this->f_forceCalc(iCurrentTime);
                velocityValuesInChain[particleNumber] += velocityPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetVelocity(velocityValuesInChain);
        }
    }
    // else do nothing
    
}

void ExternalPerturbationSinusoidal::vSetParametersForExternalPerturbation(
    std::string iType,
    std::vector<double> iParameters)
{
    // Order of parameters is e, f, g, h, ii
    // e, idx 0 -> perturbation amplitude
    // f, idx 1  -> t1 (start time)
    // g, idx 2  -> t2 (time period)
    // h, idx 3  -> t3 (end time)
    // ramp, idx 4  -> i
    // frequency = 2*pi/t2

    try
    {
        
        this->perturbationFunction = iType;

        this->amp = iParameters[0];       
        
        this->t1 = iParameters[1];

        this->t2 = iParameters[2];
        this->frequency = 2 * pi / t2;

        this->t3 = iParameters[3];

        this->ramp = iParameters[4];     
        
    }
    catch(std::exception & e)
    {
        std::cout << "Something went wrong with setting external perturbation parameters. Please check your input file" << std::endl;
        std::cout << e.what() << std::endl;

        std::terminate();
    }
    
}

