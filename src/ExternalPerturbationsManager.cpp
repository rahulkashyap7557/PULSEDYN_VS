
#pragma once
#include "../include/ExternalPerturbationsManager.h"
#include "../include/ExternalPerturbationBase.h"
#include "../include/ChainParticles.h"

std::shared_ptr<ExternalPerturbationsManager> ExternalPerturbationsManager::Create()
{
	return std::shared_ptr<ExternalPerturbationsManager>(new ExternalPerturbationsManager());
	//
}

void ExternalPerturbationsManager::AppendExternalPerturbation(std::string iType,
									std::string iExternalPerturbationVariable,
									std::string iParticleNumber, 
									std::vector<double> iParameters,
									unsigned int iSystemSize)
{
	std::shared_ptr<ExternalPerturbation> externalPerturbationToAdd = ExternalPerturbation::CreateExternalPerturbation(
		iType,
		iExternalPerturbationVariable,
		iParticleNumber, 
		iParameters,
		iSystemSize);
	this->externalPerturbations.push_back(externalPerturbationToAdd);
}

void ExternalPerturbationsManager::ApplyExternalPerturbationsAndCalculateUpdatedAccelerations(
	std::shared_ptr<ChainParticles> ioChainParticles, 
	double iCurrentTime, 
	std::vector<double>& oAccelerationPredicted)
{
	unsigned int numberOfParticles;
	ioChainParticles->GetSystemSize(numberOfParticles);
	std::vector<double> accPredictForSinglePerturbation(numberOfParticles, 0.);

	for (auto externalPerturbationObject : this->externalPerturbations)
	{		
		externalPerturbationObject->ApplyExternalPerturbationAndCalculateUpdatedAcceleration(
			ioChainParticles,
			iCurrentTime,
			accPredictForSinglePerturbation
		);

		for (int ii = 0; ii < oAccelerationPredicted.size(); ii++)
		{
			oAccelerationPredicted[ii] += accPredictForSinglePerturbation[ii];
		}
	}
}

ExternalPerturbationsManager::ExternalPerturbationsManager()
{
}
