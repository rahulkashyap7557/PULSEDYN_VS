#pragma once
#include "../include/allIncludes.h"
#include "../include/Logger.h"
#include <iostream>
#include "../include/ChainParticles.h"
#include "../include/ExternalPerturbationsManager.h"

InitialConditionsParser::InitialConditionsParser()
{
    BuildOutputFileMapper();
    this->mExternalPerturbations = ExternalPerturbationsManager::Create();
}

void InitialConditionsParser::BuildOutputFileMapper()
{
    outputFileNameMap.insert(std::make_pair(PositionOutputFile, "position.dat"));
    outputFileNameMap.insert((std::make_pair(VelocityOutputFile, "velocity.dat")));
    outputFileNameMap.insert((std::make_pair(AccelerationOutputFile, "acceleration.dat")));
    outputFileNameMap.insert((std::make_pair(KineticEnergyOutputFile, "ke.dat")));
    outputFileNameMap.insert((std::make_pair(PotentialEnergyOutputFile, "pe.dat")));
    outputFileNameMap.insert((std::make_pair(TotalEnergyOutputFile, "totalEnergy.dat")));
    outputFileNameMap.insert((std::make_pair(MassOutputFile, "mass.dat")));
    outputFileNameMap.insert((std::make_pair(RestartOutputFile, "restart.dat")));
}


std::shared_ptr<InitialConditionsParser> InitialConditionsParser::Create()
{
    return std::shared_ptr<InitialConditionsParser>(new InitialConditionsParser());
}

bool InitialConditionsParser::ParseParameterFile()
{
    int fileFlag = 0;

    string line, line1;
    unsigned int j = 0;
    unsigned int i;

    std::ifstream parametersFile("parameters.txt");
    std::shared_ptr<Logger> logger = Logger::InitializeOrGetLoggerSingleton();

    //if (!parametersFile)
    if (false == parametersFile.is_open())
    {
        // Turn off commenting below if you want to stop code from running without parameter file.
        //throw std::runtime_error("Can't open parameters file. Running with default values.");
        //logFile << "Can't open parameters file. Running with default values" << endl;
        std::string messageToWrite = "Can't open parameters file. Running with default values";
        logger->WriteToLog(messageToWrite);
        std::cout << messageToWrite << std::endl;
    }
    else
    {
        while (getline(parametersFile, line))
        {
            if (line == "")
            {
                std::cout << "Skipping blank line." << endl;
                std::string messageToWrite = "line " + std::to_string(j + 1) + ": Skipping blank line.";
                logger->WriteToLog(messageToWrite);
                continue;
            }
            else
            {
                std:cout << "Reading from parameter file" << endl;
                std::string messageToWrite = "line " + std::to_string(j + 1) + ": Reading from parameter file.";
                logger->WriteToLog(messageToWrite);
                std::vector<std::string> inputList;

                stringstream ss(line);
                string tmp;
                while (std::getline(ss, tmp, ' '))
                {
                    auto tmp1 = std::regex_replace(tmp, std::regex("\\s+"), "");
                    inputList.push_back(tmp1);
                }
                string a = inputList.at(0);
                string b;
                string c;
                string d;
                string e;
                string f;
                string g;
                string h;
                string ii;
                if (inputList.size() > 1)
                {
                    b = inputList.at(1);
                }

                if (inputList.size() > 2)
                {
                    c = inputList.at(2);
                }
                if (inputList.size() > 3)
                {
                    d = inputList.at(3);
                }
                if (inputList.size() > 4)
                {
                    e = inputList.at(4);
                }
                if (inputList.size() > 5)
                {
                    f = inputList.at(5);
                }
                if (inputList.size() > 6)
                {
                    g = inputList.at(6);
                }
                if (inputList.size() > 7)

                {
                    h = inputList.at(7);
                }
                if (inputList.size() > 8)
                {
                    ii = inputList.at(8);
                }
                int goodCommand = 0; // To check if valid command has been read.

                // Check if comment line is encountered
                if (a == "#")
                {
                    cout << "Encountered comment, skipping to next line." << endl;
                    
                    std::string messageToWrite = "line " + std::to_string(j + 1) + ": Encountered comment, skipping to next line.";
                    logger->WriteToLog(messageToWrite);
                    continue;
                }

                // Load model type and parameters
                if (a == "model:")
                {
                    springType = b;
                    if (inputList.size() > 2) {
                        k1 = atof((c.c_str()));
                    }
                    if (inputList.size() > 3) {
                        k2 = atof(d.c_str());
                    }
                    if (inputList.size() > 4) {
                        k3 = atof(e.c_str());
                    }

                    goodCommand = 1;

                }

                // Set up externalPerturbations
                if (a == "force:")
                {
                    std::vector<double> params;
                    params.push_back(atof(d.c_str()));
                    params.push_back(atof(e.c_str()));
                    params.push_back(atof(f.c_str()));
                    params.push_back(atof(g.c_str()));
                    params.push_back(atof(h.c_str()));

                    this->mExternalPerturbations->AppendExternalPerturbation(
                        c,
                        "force",
                        b,
                        params,
                        systemSize);

                    goodCommand = 1;
                }

                if (a == "externalperturbation:")
                {
                    std::vector<double> params;
                    params.push_back(atof(e.c_str()));
                    params.push_back(atof(f.c_str()));
                    params.push_back(atof(g.c_str()));
                    params.push_back(atof(h.c_str()));
                    params.push_back(atof(ii.c_str()));
                    
                    
                    this->mExternalPerturbations->AppendExternalPerturbation(
                        d,
                        c,
                        b, 
                        params,
                        systemSize);

                    goodCommand = 1;
                }

                // Set up dissipation
                if (a == "dissipation:")
                {
                    std::vector<double> params;
                    params.push_back(atof(c.c_str()));

                    this->mExternalPerturbations->AppendExternalPerturbation(
                        "velocitydissipation",
                        "vel",
                        b,
                        params,
                        systemSize
                    );

                    goodCommand = 1;
                }

                // Set up boundaries
                if (a == "boundary:")
                {
                    if (b == "left")
                    {
                        std::cout << "this: " << c << endl;
                        lboundary = c;
                    }
                    else if (b == "right")
                    {
                        rboundary = c;
                    }
                    else
                    {
                        std::cout << "Cannot read boundary conditions." << endl;
                        std::cout << "Continuing simulation with default boundaries" << endl;
                    }
                    goodCommand = 1;
                }

                // Set up initial perturbation
                if (a == "init:")
                {
                    // Can open initial conditions from file, if name is given
                    if (b == "file")
                    {
                        fileFlag = 1;
                        std::string fileName = c;
                        std::ifstream initFile(c.c_str());
                        if (!initFile)
                        {
                            // Turn off commenting below if you want to stop code from running without init file.
                            //throw std::runtime_error("Cannot open initial conditions file. Starting with default initial conditions");
                            //logFile << "line " << j + 1 << ":" << "Cannot open initial conditions file. Starting with default initial conditions" << endl;
                            std::string messageToWrite = "line " + std::to_string(j + 1) + ": " + "Cannot open initial conditions file. Starting with default initial conditions";
                            logger->WriteToLog(messageToWrite);
                        
                        }
                        else
                        {
                            int fileLineC = 0;
                            int partic = 0;
                            std::vector<std::string> initList(3);
                            while (getline(initFile, line1))
                            {
                                if (line1 == "")
                                {
                                    std::cout << "Blank line encountered in initial conditions file at line " << fileLineC + 1 << "." << endl;
                                    std::cout << "Skipping blank line in initial conditions file." << endl;
                                    //logFile << "Blank line encountered in initial conditions file at line " << fileLineC + 1 << "." << endl;
                                    //logFile << "Skipping blank line in initial conditions file." << endl;

                                    std::string messageToWrite = "Blank line encountered in initial conditions file at line " + std::to_string(fileLineC + 1) + ".";
                                    messageToWrite += "Skipping blank line in initial conditions file.";
                                    logger->WriteToLog(messageToWrite);
                                }
                                else
                                {
                                    stringstream ss1(line1);
                                    string tmpx, tmpv, tmpa;
                                    ss1 >> tmpx >> tmpv >> tmpa;
                                    partic = fileLineC + 1;

                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("pos");
                                    perturbationAmplitude.push_back(atof(tmpx.c_str()));
                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("vel");
                                    perturbationAmplitude.push_back(atof(tmpv.c_str()));
                                    perturbedParticles.push_back(partic);
                                    perturbationType.push_back("acc");
                                    perturbationAmplitude.push_back(atof(tmpa.c_str()));
                                    fileLineC += 1;
                                }

                            }
                            systemSize = partic;
                        }
                    }

                    else
                    {
                        perturbedParticles.push_back(atoi(b.c_str()));
                        perturbationType.push_back(c);
                        if (d == "random")
                        {
                            double low = atof(e.c_str());
                            double high = atof(f.c_str());
                            double num = low + (high - low) * (static_cast<double>(rand() % 100)) / 100;
                            perturbationAmplitude.push_back(num);
                        }
                        else
                        {
                            perturbationAmplitude.push_back(atof(d.c_str()));

                        }
                    }
                    goodCommand = 1;
                }

                // Set up masses
                if (a == "mass:")
                {
                    if (b == "all")
                    {
                        impurityParticles.push_back(0);
                    }
                    else
                    {
                        impurityParticles.push_back(atoi(b.c_str()));
                    }
                    impurityValue.push_back(atof(c.c_str()));
                    goodCommand = 1;
                }

                // Set up total run time
                if (a == "recsteps:")
                {
                    totalTime = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up system size
                if (a == "systemsize:")
                {
                    systemSize = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up time step
                if (a == "timestep:")
                {
                    dt = atof(b.c_str());
                    goodCommand = 1;
                }

                // Set up sampling steps
                if (a == "printint:")
                {
                    samplingFrequency = atoi(b.c_str());
                    sampleFlag = 1;
                    goodCommand = 1;
                }

                // Set up integration method
                if (a == "method:")
                {
                    method = b;
                    goodCommand = 1;
                }

                if (goodCommand == 0)
                {
                    cout << "Command not recognized. Please make sure the commands are entered correctly in the code." << endl;
                    cout << "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment." << endl;
                    cout << "Make sure that every comment line is preceded with the comment symbol i.e. #." << endl;
                    
                    std::string messageToWrite = "line " + std::to_string(j + 1) + ":" + "Command not recognized. Please make sure the commands are entered correctly in the code.\n";
                    messageToWrite += "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment.\n";
                    messageToWrite += "Make sure that every comment line is preceded with the comment symbol i.e. #.\n";
                    logger->WriteToLog(messageToWrite);
                    //logFile << "line " << j + 1 << ":" << "Command not recognized. Please make sure the commands are entered correctly in the code." << endl;
                    //logFile << "If you want to write comments in the parameter file, make sure that you start the line with a #, leave a space and then write your comment." << endl;
                    //logFile << "Make sure that every comment line is preceded with the comment symbol i.e. #." << endl;

                    return 0;
                    //logFile.close();
                }
                cout << line << endl;
                j++;
            }
        }
    }
    //logFile.close();
    parametersFile.close();
    cout << "Read in parameters" << endl;

    if (fileFlag == 1)
    {
        cout << "Read initial conditions from file" << endl;
    }

    return true;
}

void InitialConditionsParser::SetVariables(Simulation& oSimParams, 
    std::shared_ptr<ChainParticles>& oChainparticles)
{
    oChainparticles->SetSystemSize(systemSize);
    double defaultx = 0.;
    double defaultv = 0.;
    double defaulta = 0.;

    // Next create an object with info about the springs
    if (lboundary == "periodic")
    {
        rboundary = lboundary;
    }
    if (rboundary == "periodic")
    {
        lboundary = rboundary;
    }

    // Initialize positions, velocities and accelerations
    oChainparticles->SetLBoundary(lboundary);
    oChainparticles->SetRBoundary(rboundary);

    oChainparticles->SetInitialPerturbations(perturbedParticles, perturbationAmplitude, perturbationType);
    oChainparticles->SetImpurities(this->impurityParticles, this->impurityValue);
    // Create simulation object
    
    if (sampleFlag == 0)
    {
        samplingFrequency = 1 / dt;
        if (samplingFrequency < 1)
        {
            samplingFrequency = 10;
        }
    }

    oSimParams.SettimeStep(dt);
    oSimParams.SetsamplingFrequency(samplingFrequency);
    oSimParams.SettotalTime(totalTime);
    oSimParams.Setmethod(method);
    oSimParams.SetsystemSize(systemSize);
}

std::string InitialConditionsParser::GetSpringType()
{
    return springType;
}


// Check if any existing files with same names that we wish to write to
bool InitialConditionsParser::CheckAndDeleteExistingOutputFiles()
{
    for (std::map<OUTPUT_FILE_NAMES, string>::iterator iter = outputFileNameMap.begin();
         iter != outputFileNameMap.end();
         iter++)
    {
        std::string fileName = iter->second;
        std::string errOut;

        if(ifstream(fileName.c_str()))
        {
            std::cout << "Existing file will be deleted - "<< fileName << std::endl;
            if( remove( fileName.c_str() ) != 0 )
            {
                errOut = "Error deleting file " + fileName;
                perror(  fileName.c_str() );
                std::cout << std::endl;
            }
            else
            {
                errOut = fileName + " successfully deleted.";
                puts( errOut.c_str() );
                std::cout << std::endl;
            }
        }
    }
    // Check if file exists and delete if it does.

    // TODO: for now returning true, but should return if failed to delete files already present
    return true;

}

double InitialConditionsParser::GetK1()
{
    return k1;
}

double InitialConditionsParser::GetK2()
{
    return k2;
}

double InitialConditionsParser::GetK3()
{
    return k3;
}

