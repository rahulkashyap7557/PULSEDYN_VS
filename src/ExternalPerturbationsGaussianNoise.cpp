#pragma once
#include <iostream>
#include <functional>
#include "../include/ExternalPerturbationsGaussianNoise.h"
#include "../include/ChainParticles.h"

ExternalPerturbationsGaussianNoise::ExternalPerturbationsGaussianNoise(
	std::string iType, 
	std::string iExternalPerturbationVariable, 
	std::string iParticleNumber, 
	std::vector<double> iParameters, 
	unsigned int iSystemSize)
{
	SetParticlesToApplyPerturbationOn(iParticleNumber, iSystemSize);
	SetExternalPerturbationVariable(iExternalPerturbationVariable);
	SetParametersForExternalPerturbation(iType, iParameters);
}

void ExternalPerturbationsGaussianNoise::CalculateRandomPerturbation(double& oPerturbationValue)
{
    oPerturbationValue = this->mDistribution(this->mGenerator);
}

void ExternalPerturbationsGaussianNoise::vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
	std::shared_ptr<ChainParticles> ioChainParticles, 
	double iCurrentTime, 
	std::vector<double>& oPredictedAcceleration)
{
	unsigned int systemSize;
	ioChainParticles->GetSystemSize(systemSize);

    if (this->mExternalPerturbationVariable == "force")
    {
//#pragma omp parallel num_threads(8)
        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateRandomPerturbation(perturbation);
                oPredictedAcceleration[particleNumber] = perturbation;
            }
            // else do nothing
        }
    }
    else if (this->mExternalPerturbationVariable == "pos")
    {
        std::vector<double> positionPerturbation(systemSize, 0.);

        std::vector<double> positionValuesInChain;
        ioChainParticles->GetPosition(positionValuesInChain);

        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateRandomPerturbation(perturbation);
                positionPerturbation[particleNumber] = perturbation;
                positionValuesInChain[particleNumber] += positionPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetPosition(positionValuesInChain);
        }
    }
    else if (this->mExternalPerturbationVariable == "vel")
    {
        std::vector<double> velocityPerturbation(systemSize, 0.);

        std::vector<double> velocityValuesInChain;
        ioChainParticles->GetVelocity(velocityValuesInChain);

        for (auto const& particleNumber : this->mParticlesToApplyExternalPerturbationOn)
        {
            if (iCurrentTime > this->mStartTime && iCurrentTime < this->mEndTime)
            {
                double perturbation;
                this->CalculateRandomPerturbation(perturbation);
                velocityPerturbation[particleNumber] = perturbation;
                velocityValuesInChain[particleNumber] += velocityPerturbation[particleNumber];
            }
            // else do nothing

            ioChainParticles->SetVelocity(velocityValuesInChain);
        }
    }
    // else do nothing
}

void ExternalPerturbationsGaussianNoise::vSetParametersForExternalPerturbation(
	std::string iType, 
	std::vector<double> iParameters)
{
	// e, f, h, ii
	// e, idx 0 -> perturbation amplitude
	// f, idx 1  -> mu
	// g, idx 2  -> sigma
	// h, idx 3  -> t1 (start time)
	// idx 4  -> t2 (end time)
	try
	{
		this->mPertubationFunction = iType;
		
		this->mDrivingAmplitude = iParameters[0];

		this->mMu = iParameters[1];

		this->mSigma = iParameters[2];

		this->mStartTime = iParameters[3];

		this->mEndTime = iParameters[4];

        std::default_random_engine generator;
        this->mDistribution = std::normal_distribution<double>(this->mMu, this->mSigma);

        std::random_device r;
        std::seed_seq seed2{ r() };

        std::mt19937 e2(seed2);
        this->mGenerator = e2;
	}
	catch (std::exception& e)
	{
		std::cout << "Something went wrong with setting external perturbation parameters for gaussian noise. Please check your input file" << std::endl;
		std::cout << e.what() << std::endl;

		std::terminate();
	}
}
