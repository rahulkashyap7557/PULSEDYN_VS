/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../include/allIncludes.h"


// All the simulation functions are written in the header file because

Simulation::Simulation()
{
    //ctor
    timeStep = 0.00001;
    samplingFrequency = int(1/timeStep);
    totalTime = 10000;
    method = "gear5";
    systemSize = 100;

    mIntegrator = Integrator::Create("gear5");

}

void Simulation::Setmethod(string val)
{
    method = val;
}

void Simulation::SetsystemSize(int val)
{
    systemSize = val;
}

void Simulation::WriteToFile()
{
    std::shared_ptr<Output> outputFileWriter = Output::CreateOrGetSingleton();
    outputFileWriter->WriteToFile(positionCache,
        velocityCache,
        accelerationCache,
        kineticEnergyCache,
        potentialEnergyCache,
        massCache,
        totalEnergyCache);
 
}

void Simulation::WriteRestartFiles(std::vector<double>& position, std::vector<double>& velocity, std::vector<double> acceleration)
{
    std::shared_ptr<Output> outputFileWriter = Output::CreateOrGetSingleton();

    for (int ii = 0; ii < systemSize; ii++)
    {
        outputFileWriter->writeToResFile(position[ii], false);
        outputFileWriter->writeToResFile(velocity[ii], false);
        outputFileWriter->writeToResFile(acceleration[ii], true);
    }
}

void Simulation::SaveToCache(std::vector<double>& positionTemp, 
    std::vector<double>& velocityTemp, 
    std::vector<double>& accelerationTemp, 
    std::vector<double>& kineticEnergy, 
    std::vector<double>& potentialEnergy,
    std::vector<double>& mass, 
    double& totalEnergy)
{
    this->positionCache.push_back(positionTemp);
    this->velocityCache.push_back(velocityTemp);
    this->accelerationCache.push_back(accelerationTemp);
    this->kineticEnergyCache.push_back(kineticEnergy);
    this->potentialEnergyCache.push_back(potentialEnergy);
    this->massCache.push_back(mass);
    this->totalEnergyCache.push_back(totalEnergy);

}

void Simulation::ClearVariableCache()
{
    this->positionCache = std::vector<std::vector<double>>();
    this->velocityCache = std::vector<std::vector<double>>();
    this->accelerationCache = std::vector<std::vector<double>>();
    this->kineticEnergyCache = std::vector<std::vector<double>>();
    this->massCache = std::vector<std::vector<double>>();
    this->totalEnergyCache = std::vector<double>();
}



