#include "../include/CoulombPotential.h"
#include "../include/Output.h"
CoulombPotential::CoulombPotential()
{
    //ctor
    k1 = 0.0;
    k2 = 0.0;
    k3 = 1.0;
}

void CoulombPotential::CalculateAcceleration(std::shared_ptr<ChainParticles> & iChainParticles)
    {
        string lb, rb;

        unsigned int systemSize = 0;
        iChainParticles->GetSystemSize(systemSize);
        iChainParticles->GetLeftBoundary(lb);
        iChainParticles->GetRightBoundary(rb);
        unsigned int systemSizeMinus1 = systemSize - 1;

        std::vector<double> position;
        iChainParticles->GetPosition(position);

        std::vector<double> massVector;
        iChainParticles->GetMass(massVector);

        std::vector<double> accel(systemSize, 0.0);
        // Calculate the non boundry particles first and then figure out the boundaries

        for (int ii = 1; ii < systemSizeMinus1; ii++)
        {
            double fac1;
            double fac2;
            int k = ii + 1;
            int j = ii - 1;
            fac2 = position[ii] - position[k];
            fac1 = position[ii] - position[j];

            // Write the nonlinear term
            double rLeft = fac1 - k3;
            double rRight = -1.0 * fac2 - k3;

            double nonlinearTerm = (1.0 / (rLeft * rLeft)) - (1.0 / (rRight * rRight));
            double a = -1.0 * k1 * (fac1 + fac2) - k2 * nonlinearTerm;
            a = a / massVector[ii];
            accel[ii] = a;
        }

        // Now calculate accceleration for left boundary;
        double fac1 = f_leftBoundaryConditionsAccel(iChainParticles);
        double fac2 = position[0] - position[1];

        // Write the nonlinear term
        double rLeft = fac1 - k3;
        double rRight = -1.0 * fac2 - k3;

        double nonlinearTerm = (1.0 / (rLeft * rLeft)) - (1.0 / (rRight * rRight));
        double a = -1.0 * k1 * (fac1 + fac2) - k2 * nonlinearTerm;
        a = a / massVector[0];
        accel[0] = a;

        // Next calculate the acceleration for right most boundary
        fac1 = position[systemSizeMinus1] - position[systemSizeMinus1 - 1];
        fac2 = f_rightBoundaryConditionsAccel(iChainParticles);

        // Write the nonlinear term
        rLeft = fac1 - k3;
        rRight = -1.0 * fac2 - k3;

        nonlinearTerm = (1.0 / (rLeft * rLeft)) - (1.0 / (rRight * rRight));
        a = -1.0 * k1 * (fac1 + fac2) - k2 * nonlinearTerm;
        a = a / massVector[systemSizeMinus1];
        accel[systemSizeMinus1] = a;
        accel[systemSizeMinus1] = a;

        // Set acceleration back in the chainParticles object
        iChainParticles->SetAccel(accel);
    }

double CoulombPotential::TotalPotentialEnergy(std::shared_ptr<ChainParticles> iChainParticles,
    std::vector<double>& oPotentialEnergy)
{
    std::vector<double> position;
    iChainParticles->GetPosition(position);
    unsigned int systemSize;
    iChainParticles->GetSystemSize(systemSize);
    unsigned int systemSizeMinus1 = systemSize - 1;

    double totalPotentialEnergy = 0.0;

    // Do the inner particles and then do the boundaries separately
    for (unsigned int ii = 1; ii < systemSize; ii++)
    {
        unsigned int jj = ii - 1;
        double dx = position[ii] - position[jj];
        double fac1 = dx - k3;
        double fac2 = fac1 * fac1;
        oPotentialEnergy[ii] = 0.5 * k1 * fac2 + k2 * pow(fac1, -1);        
    }

    // Now add the potential energy of the first spring
    double dxLeftBoundary = f_leftBoundaryConditionsPe(iChainParticles);
    double fac1 = dxLeftBoundary - k3;
    double fac2 = fac1 * fac1;
    oPotentialEnergy[0] = 0.5 * k1 * fac2 + k2 * pow(fac1, -1);

    // Add potential energy of the right most spring
    double dxRightBoundary = -1.0*f_rightBoundaryConditionsPe(iChainParticles);
    fac1 = dxRightBoundary - k3;
    fac2 = fac1 * fac1;
    oPotentialEnergy[systemSize] = 0.5 * k1 * fac2 + k2 * pow(fac1, -1);

    for (int ii = 0; ii < oPotentialEnergy.size(); ii++)
    {
        totalPotentialEnergy += oPotentialEnergy[ii];
    }

    return totalPotentialEnergy;
}
