#pragma once
#include "../include/ExternalPerturbationBase.h"
#include "../include/ExternalPerturbationsFactory.h"
#include "../include/ChainParticles.h"
#include <iostream>
#include <exception>

std::shared_ptr<ExternalPerturbation> ExternalPerturbation::CreateExternalPerturbation(
	std::string iType,
	std::string iExternalPerturbationVariable,
	std::string iParticleNumber,
	std::vector<double> iParameters,
	unsigned int iSystemSize)
{
	std::shared_ptr<ExternalPerturbation> perturbationToReturn;

	if (iType == "sine" || iType == "cosine")
	{
		perturbationToReturn = ExternalPerturbationsFactory::ReturnSinusoidalExternalPerturbation(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber, 
			iParameters,
			iSystemSize);
	}
	else if (iType == "gaussiannoise")
	{
		perturbationToReturn = ExternalPerturbationsFactory::ReturnGaussianNoiseExternalPerturbation(
			iType, 
			iExternalPerturbationVariable,
			iParticleNumber, 
			iParameters,
			iSystemSize);
	}
	else if (iType == "velocitydissipation")
	{
		perturbationToReturn = ExternalPerturbationsFactory::ReturnVelocityDampingExternalPerturbation(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber,
			iParameters,
			iSystemSize);
	}
	else if (iType == "sawtooth")
	{
		perturbationToReturn = ExternalPerturbationsFactory::ReturnSawtoothExternalPerturbation(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber,
			iParameters,
			iSystemSize
		);
	}
	else
	{
		std::cout << "No recognized external perturbation. Please check your parameter file." << std::endl;
		std::terminate();
	}

	return perturbationToReturn;

}

//void ExternalPerturbation::ApplyPerturbation(std::shared_ptr<ChainParticles> ioChainParticles)
//{
//	vApplyPerturbation(ioChainParticles);
//}

void ExternalPerturbation::SetParticlesToApplyPerturbationOn(
	std::string& iParticleNumber,
	unsigned int iSystemSize)
{
	if (iParticleNumber == "all")
	{
		for (int ii = 0; ii < iSystemSize; ii++)
		{
			this->mParticlesToApplyExternalPerturbationOn.push_back(ii);
		}
	}
	else
	{
		try
		{
			this->mParticlesToApplyExternalPerturbationOn.push_back(atoi(iParticleNumber.c_str()));
		}
		catch (std::exception& e)
		{
			std::cout << "incorrect particle number to apply force on" << std::endl;
			std::cout << "Exiting program now" << std::endl;
			std::cout << e.what() << std::endl;
			std::terminate();
		}
	}
}

void ExternalPerturbation::ApplyExternalPerturbationAndCalculateUpdatedAcceleration(
	std::shared_ptr<ChainParticles> ioChainParticles, 
	double iCurrentTime, 
	std::vector<double>& oPredictedAcceleration)
{
	vApplyExternalPerturbationAndCalculateUpdatedAcceleration
	(
		ioChainParticles,
		iCurrentTime,
		oPredictedAcceleration
	);
}

void ExternalPerturbation::SetExternalPerturbationVariable(std::string iExternalPerturbationVariable)
{
	this->mExternalPerturbationVariable = iExternalPerturbationVariable;
}

void ExternalPerturbation::SetParametersForExternalPerturbation(
	std::string iType, 
	std::vector<double> iParameters)
{
	vSetParametersForExternalPerturbation(
		iType,
		iParameters
	);
}
