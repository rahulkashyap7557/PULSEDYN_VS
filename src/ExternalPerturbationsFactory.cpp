#pragma once
#include "../include/ExternalPerturbationsFactory.h"
#include "../include/ExternalPerturbationSinusoidal.h"
#include "../include/ExternalPerturbationsGaussianNoise.h"
#include "../include/ExternalPerturbationsVelocityDamping.h"
#include "../include/ExternalPerturbationsSawtooth.h"

std::shared_ptr<ExternalPerturbation> ExternalPerturbationsFactory::ReturnSinusoidalExternalPerturbation(
	std::string iType, 
	std::string iExternalPerturbationVariable,
	std::string iParticleNumber, 
	std::vector<double> iParameters,
	unsigned int iSystemSize)
{
	return std::shared_ptr<ExternalPerturbation>(
		new ExternalPerturbationSinusoidal(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber, 
			iParameters,
			iSystemSize));
}

std::shared_ptr<ExternalPerturbation> ExternalPerturbationsFactory::ReturnGaussianNoiseExternalPerturbation(
	std::string iType, 
	std::string iExternalPerturbationVariable,
	std::string iParticleNumber, 
	std::vector<double> iParameters,
	unsigned int iSystemSize)
{
	return std::shared_ptr<ExternalPerturbation>(
		new ExternalPerturbationsGaussianNoise(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber, 
			iParameters,
			iSystemSize
		));
}

std::shared_ptr<ExternalPerturbation> ExternalPerturbationsFactory::ReturnVelocityDampingExternalPerturbation(
	std::string iType, 
	std::string iExternalPerturbationVariable, 
	std::string iParticleNumber,
	std::vector<double> iParameters, 
	unsigned int iSystemSize)
{
	return std::shared_ptr<ExternalPerturbation>(
		new ExternalPerturbationsVelocityDamping(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber,
			iParameters,
			iSystemSize
		));
}

std::shared_ptr<ExternalPerturbation> ExternalPerturbationsFactory::ReturnSawtoothExternalPerturbation(
	std::string iType, 
	std::string iExternalPerturbationVariable, 
	std::string iParticleNumber, 
	std::vector<double> iParameters, 
	unsigned int iSystemSize)
{
	return std::shared_ptr<ExternalPerturbation>(
		new ExternalPerturbationsSawtooth(
			iType,
			iExternalPerturbationVariable,
			iParticleNumber,
			iParameters,
			iSystemSize
		));
}


