/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#include "../include/Output.h"
#include <thread>

std::shared_ptr<Output> Output::singleton = nullptr;

Output::~Output()
{
    this->positionFile.close();
    this->velocityFile.close();
    this->accelerationFile.close();
    this->keFile.close();
    this->peFile.close();
    this->totenFile.close();
    this->restartFile.close();
    this->massFile.close();
}

std::shared_ptr<Output> Output::CreateOrGetSingleton()
{
    if (nullptr == singleton)
    {
        return std::shared_ptr<Output>(new Output());
    }
    else
    {
        return singleton;
    }
    
}

Output::Output()
{
    this->positionFileName = "position.dat";
    this->velocityFileName = "velocity.dat";
    this->accelerationFileName = "acceleration.dat";
    this->keFileName = "ke.dat";
    this->peFileName = "pe.dat";
    this->totenFileName = "totalEnergy.dat";
    this->restartFileName = "restart.dat";
    this->massFileName = "mass.dat";
    OpenFiles();
}



void Output::OpenFiles()
{
    this->positionFile.open(positionFileName.c_str(), std::ofstream::out | std::ofstream::app); // Uncomment this line if you want to append to files instead of overwriting
    this->positionFile.precision(15);

    this->velocityFile.open(velocityFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->velocityFile.precision(15);

    this->accelerationFile.open(accelerationFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->accelerationFile.precision(15);

    this->keFile.open(keFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->keFile.precision(15);

    this->peFile.open(peFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->peFile.precision(15);

    this->totenFile.open(totenFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->totenFile.precision(15);

    this->restartFile.open(restartFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->restartFile.precision(15);

    this->massFile.open(massFileName.c_str(), std::ofstream::out | std::ofstream::app);
    this->massFile.precision(15);
}

void Output::writeToPeFile(double val, bool endline)
{
    this->peFile << val;

    if (endline)
    {
        peFile << '\n';
    }
    else
    {
        peFile << '\t';
    }

}

void Output::writeToTotFile(double val, bool endline)
{
    this->totenFile << val;

    if (endline)
    {
        totenFile << '\n';
    }
    else
    {
        totenFile << '\t';
    }

}
//
void Output::writeToResFile(double& val, bool endline)
{
    this->restartFile << val;

    if (endline)
    {
        restartFile << '\n';
    }
    else
    {
        restartFile << '\t';
    }
}

void Output::writeToVelFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->velocityFile << iInputValues[ii];

        int numVals = iInputValues.size();
        
        if (ii < numVals - 1)
        {
            velocityFile << '\t';
        }
        else
        {
            velocityFile << '\n';
        }
    }
}

void Output::writeToPosFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->positionFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            positionFile << '\t';
        }
        else
        {
            positionFile << '\n';
        }
    }
}

void Output::writeToAccFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->accelerationFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            accelerationFile << '\t';
        }
        else
        {
            accelerationFile << '\n';
        }
    }
}

void Output::writeToKeFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->keFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            keFile << '\t';
        }
        else
        {
            keFile << '\n';
        }
    }
}

void Output::writeToPeFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->peFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            peFile << '\t';
        }
        else
        {
            peFile << '\n';
        }
    }
}

void Output::writeToMassFile(std::vector<double>& iInputValues)
{
    for (int ii = 0; ii < iInputValues.size(); ii++)
    {
        this->massFile << iInputValues[ii];

        int numVals = iInputValues.size();

        if (ii < numVals - 1)
        {
            massFile << '\t';
        }
        else
        {
            massFile << '\n';
        }
    }
}

void Output::WriteToFile(std::vector<std::vector<double>>& positionTemp,
    std::vector<std::vector<double>>& velocityTemp,
    std::vector<std::vector<double>>& accelerationTemp,
    std::vector<std::vector<double>>& kineticEnergy,
    std::vector<std::vector<double>>& potentialEnergy,
    std::vector<std::vector<double>>& mass,
    std::vector<double>& totalEnergy)
{
    unsigned int cacheSize = velocityTemp.size();

#pragma omp parallel num_threads(8)
    {
#pragma omp sections
        {
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToPosFile(positionTemp[ii]);
                }
                
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToVelFile(velocityTemp[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToAccFile(accelerationTemp[ii]);
                }
                
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToKeFile(kineticEnergy[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToPeFile(potentialEnergy[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToMassFile(mass[ii]);
                }
            }
#pragma omp section
            {
                for (int ii = 0; ii < cacheSize; ii++)
                {
                    writeToTotFile(totalEnergy[ii], true);
                }
            }
        }
    }
}
