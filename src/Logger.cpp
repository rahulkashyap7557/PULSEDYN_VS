#include "../include/Logger.h"

std::shared_ptr<Logger> Logger::singleton = nullptr;

std::shared_ptr<Logger> Logger::InitializeOrGetLoggerSingleton()
{
	if (nullptr == singleton)
	{
		singleton = std::shared_ptr<Logger>(new Logger());
	}
	// else don't initialize

	return singleton;
}

void Logger::WriteToLog(std::string iLogMessage)
{
	logFile << iLogMessage << std::endl;
}

Logger::~Logger()
{
	CloseLogFile();
}

Logger::Logger()
{
	OpenLogFile();	
}

void Logger::OpenLogFile()
{
	logFile.open(logFileName, std::ofstream::out | std::ofstream::app);
}

void Logger::CloseLogFile()
{
	logFile.close();
}
