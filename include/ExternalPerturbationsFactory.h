#pragma once
#include <memory>
#include <string>
#include <vector>

class ExternalPerturbation;

class ExternalPerturbationsFactory
{
public:
	static std::shared_ptr<ExternalPerturbation> ReturnSinusoidalExternalPerturbation(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

	static std::shared_ptr<ExternalPerturbation> ReturnGaussianNoiseExternalPerturbation(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

	static std::shared_ptr<ExternalPerturbation> ReturnVelocityDampingExternalPerturbation(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

	static std::shared_ptr<ExternalPerturbation> ReturnSawtoothExternalPerturbation(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);
};