#ifndef OUTPUTFILENAMES_H
#define OUTPUTFILENAMES_H

// Eventually this will be upgraded to a full class
enum OUTPUT_FILE_NAMES
{
    PositionOutputFile,
    VelocityOutputFile,
    AccelerationOutputFile,
    KineticEnergyOutputFile,
    PotentialEnergyOutputFile,
    TotalEnergyOutputFile,
    MassOutputFile,
    RestartOutputFile
};

#endif // OUTPUTFILENAMES_H
