/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef OUTPUT_H
#define OUTPUT_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include <memory>

class Output
{
    public:
        ~Output();
        
       static std::shared_ptr<Output> CreateOrGetSingleton();
       
       void writeToPeFile(double val, bool endline);
       void writeToTotFile(double val, bool endline);
       void writeToResFile(double& val, bool endline);

       void writeToVelFile(std::vector<double>& iInputValues);
       void writeToPosFile(std::vector<double>& iInputValues);
       void writeToAccFile(std::vector<double>& iInputValues);
       void writeToKeFile(std::vector<double>& iInputValues);
       void writeToPeFile(std::vector<double>& iInputValues);
       void writeToMassFile(std::vector<double>& iInputValues);
       void WriteToFile(std::vector<std::vector<double>>& positionTemp,
           std::vector<std::vector<double>>& velocityTemp,
           std::vector<std::vector<double>>& accelerationTemp,
           std::vector<std::vector<double>>& kineticEnergy,
           std::vector<std::vector<double>>& potentialEnergy,
           std::vector<std::vector<double>>& mass,
           std::vector<double>& totalEnergy);

    protected:

    private:
        Output();
        void OpenFiles();
        std::string positionFileName;
        std::string velocityFileName;
        std::string accelerationFileName;
        std::string keFileName;
        std::string peFileName;
        std::string totenFileName;
        std::string restartFileName;
        std::string massFileName;

        std::ofstream positionFile;
        std::ofstream velocityFile;
        std::ofstream accelerationFile;
        std::ofstream keFile;
        std::ofstream peFile;
        std::ofstream totenFile;
        std::ofstream restartFile;
        std::ofstream massFile;

        static std::shared_ptr<Output> singleton;
};

#endif // OUTPUT_H
