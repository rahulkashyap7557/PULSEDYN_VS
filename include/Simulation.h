/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#pragma once
#include "../include/ExternalPerturbationsManager.h"

#ifndef SIMULATION_H
#define SIMULATION_H
#include "Particle.h"
#include "fpuPotential.h"
#include "todaPotential.h"
#include "morsePotential.h"
#include "lennardJonesPotential.h"
#include "Output.h"
#include "ExternalPerturbationSinusoidal.h"
#include "Gear5Integrator.h"
#include "Integrator.h"
#include "ChainParticles.h"


using namespace std;

class Simulation
{
public:
    /** Default constructor */
    Simulation();

    /** Access timeStep
         * \return The current value of timeStep
         */
    double GettimeStep() 
    { 
        return timeStep;
    }
    /** Set timeStep
         * \param val New value to set
         */
    void SettimeStep(double val) 
    { 
        timeStep = val;
        mIntegrator->SetTimeStep(val); 
    }
    /** Access samplingFrequency
         * \return The current value of samplingFrequency
         */
    int GetsamplingFrequency() { return samplingFrequency; }
    /** Set samplingFrequency
         * \param val New value to set
         */
    void SetsamplingFrequency(int val) { samplingFrequency = val; }
    /** Access totalTime
         * \return The current value of totalTime
         */
    int GettotalTime() { return totalTime; }
    /** Set totalTime
         * \param val New value to set
         */
    void SettotalTime(int val) { totalTime = val; }

    string Getmethod(){return method; }

    void Setmethod(string val);

    int GetsystemSize(){return systemSize; }

    void SetsystemSize(int val);

    void SetSpringType(std::string iStringType) { potentialType = iStringType; }

    template <class PotType>
    void f_velocityVerlet(std::shared_ptr<ChainParticles>&, 
        std::vector<PotType> &chainPotential);

    template <class PotType>
    void f_gear5(std::shared_ptr<ChainParticles>&, 
        std::vector<PotType> &chainPotential, 
        std::shared_ptr<ExternalPerturbationsManager> iExternalPerturbationManager);//, Potential &chainPotential[])


    void f_rk4();
    void f_rk4Adaptive();

    template <class PotType>
    void f_startSim(std::shared_ptr<ChainParticles>& chainParticles, 
        std::vector<PotType> &chainPotential, 
        std::shared_ptr<ExternalPerturbationsManager> iExternalPerturbationManager);

protected:

private:
    double timeStep; //!< Member variable "timeStep"
    int samplingFrequency; //!< Member variable "samplingFrequency"
    int totalTime; //!< Member variable "totalTime"
    unsigned int systemSize;
    string method;
    std::string potentialType;

    std::shared_ptr<Integrator> mIntegrator;

    unsigned int maxCacheSize = 1000;
    unsigned int currentCacheSize = 0;
    std::vector<std::vector<double>> positionCache;
    std::vector<std::vector<double>> velocityCache;
    std::vector<std::vector<double>> accelerationCache;
    std::vector<std::vector<double>> massCache;
    std::vector<std::vector<double>> kineticEnergyCache;
    std::vector<std::vector<double>> potentialEnergyCache;
    std::vector<double> totalEnergyCache;

    void WriteToFile();

    void WriteRestartFiles(
        std::vector<double>& position,
        std::vector<double>& velocity,
        std::vector<double> acceleration
    );

    void SaveToCache(
        std::vector<double>& positionTemp,
        std::vector<double>& velocityTemp,
        std::vector<double>& accelerationTemp,
        std::vector<double>& kineticEnergy,
        std::vector<double>& potentialEnergy,
        std::vector<double>& mass,
        double& totalEnergy
        );

    void ClearVariableCache();

};


template <class PotType>
void Simulation::f_startSim(std::shared_ptr<ChainParticles>& chainParticles, 
    std::vector<PotType> &chainPotential, 
    std::shared_ptr<ExternalPerturbationsManager> iExternalPerturbationManager)//, Potential &chainPotential[])
{
    if (method == "gear5")
    {
        f_gear5(chainParticles, chainPotential, iExternalPerturbationManager);
    }
    else if (method == "velocityverlet")
    {
        f_velocityVerlet(chainParticles, chainPotential);
    }
}


// Gear solver
template <class PotType>
void Simulation::f_gear5(std::shared_ptr<ChainParticles>& chainParticles, 
    std::vector<PotType> &chainPotential, 
    std::shared_ptr<ExternalPerturbationsManager> iExternalPerturbationManager)//, Potential &chainPotential[])
{
    cout << "Gear 5th order" << endl;

    // 05/22/2022 RKP: Leaving these in because I am planning to clean up the integrator classes
    // They will eventually be integrated into their own library
    mIntegrator = Integrator::Create("gear5");

    mIntegrator->SetTimeStep(timeStep);

    // Initialize variables needed for the Gear 5th order algorithm

    // These three are higher order terms used to evolve position, velocity and acceleration
    // in the Gear algorithm.
    std::vector<double> position3;
    // 05/22/2022 RKP: Leaving these in because these are gcc specific and I may need to revive it
    //double position3[systemSize]; //__attribute__((aligned));
    //double position4[systemSize]; //__attribute__((aligned));
    std::vector<double> position4;
    std::vector<double> position5;
    std::vector<double> accelerationPredict;
    std::vector<double> deltaR;

    // 05/22/2022 RKP: Leaving these in because these are gcc specific and I may need to revive it
    //double position5[systemSize];
    //double accelerationPredict[systemSize]; // __attribute__((aligned)); // This is the acceleration calculated by the Predictor step of the Gear algorithm
    //double deltaR[systemSize]; // __attribute__((aligned)); // This is the quantity used by the corrector algorithm to adjust accelerationPredict to follow energy conservation

    // Temporary variables used to update the position, velocity and acceleration variables
    // 05/22/2022 RKP: Leaving these in because these are gcc specific and I may need to revive it
    //double positionTemp[systemSize]; // __attribute__((aligned));
    std::vector<double> positionTemp;
    std::vector<double> velocityTemp;
    std::vector<double> accelerationTemp;
    std::vector<double> externalPerturbationValues;
    std::vector<double> mass;
    std::vector<double> kineticEnergy;
    std::vector<double> potentialEnergy(systemSize + 1, 0.);

    // 05/22/2022 RKP: Leaving these in because these are gcc specific and I may need to revive it
    //double velocityTemp[systemSize]; // __attribute__((aligned));
    //double accelerationTemp[systemSize]; // __attribute__((aligned));
    //double externalPerturbationValues[systemSize]; // __attribute__((aligned));


    // Initialize temporary variables
    unsigned int i;

    chainParticles->GetPosition(positionTemp);
    chainParticles->GetVelocity(velocityTemp);
    chainParticles->GetAccel(accelerationTemp);
    chainParticles->GetMass(mass);
    chainParticles->GetKe(kineticEnergy);
    
    for (i = 0; i < systemSize; i++)
    {
        accelerationPredict.push_back(0.);
        deltaR.push_back(0.);
        position3.push_back( 0.);
        position4.push_back(0.);
        position5.push_back(0.);
        externalPerturbationValues.push_back(0.);
    }
    // Initialize KE variable to be calculated and written to file
    double totalKineticEnergy = 0.;

    // Write initial data (at t = 0) to file
    chainParticles->CalculateKE();
    chainParticles->GetKe(kineticEnergy);

    chainParticles->GetTotalKineticEnergy(totalKineticEnergy);

    // Calculate potential and total energy of the chain

    double totalPotentialEnergy = 0.;
    totalPotentialEnergy = chainPotential[0].TotalPotentialEnergy(chainParticles, potentialEnergy);

    double totalEnergy = 0.;
    totalEnergy = totalKineticEnergy + totalPotentialEnergy; // calculate total energy

    
    SaveToCache(positionTemp,
        velocityTemp,
        accelerationTemp,
        kineticEnergy,
        potentialEnergy,
        mass,
        totalEnergy);

    // Set up solver variables

    // The same coefficients as the Predictor step. They are used here as well
    // Initialized here so that they are not reinitialized repeatedly inside time loop
    double c[5];
    c[0] = timeStep;
    c[1] = c[0]*timeStep/2.;
    c[2] = c[1]*timeStep/3.;
    c[3] = c[2]*timeStep/4.;
    c[4] = c[3]*timeStep/5.;

    // Calculate the inverses of c[i] to use later as multiplication rather than dividing to increase speed.
    // Initialized here so that they are not reinitialized repeatedly inside time loop
    double cInverse[5];
    cInverse[0] = 1/timeStep;
    cInverse[1] = 1/c[1];
    cInverse[2] = 1/c[2];
    cInverse[3] = 1/c[3];
    cInverse[4] = 1/c[4];

    // Initialize coefficients for the Corrector step to adjust the position, velocity and acceleration variables according to Corrector step
    // Initialized here so that they are not reinitialized repeatedly inside time loop
    double a[6];
    a[0] = 3./20;
    a[1] = 251./360;
    a[2] = 1.;
    a[3] = 11./18;
    a[4] = 1./6;
    a[5] = 1./60;

    double tCurrent = 0.;

    // Now start simulation loop
    int n = 1; // Counter for total time
    int n1 = 1; // Counter for data write times

    while (n < totalTime)
    {
        tCurrent += timeStep;
        // Calculate dynamical variables
        for (i = 0; i < systemSize; i++)
        {
            positionTemp[i] += c[0]*velocityTemp[i] + c[1]*accelerationTemp[i] + c[2]*position3[i] + c[3]*position4[i] + c[4]*position5[i];
            velocityTemp[i] += c[0]*accelerationTemp[i] + c[1]*position3[i] + c[2]*position4[i] + c[3]*position5[i];
            accelerationTemp[i] += c[0]*position3[i] + c[1]*position4[i] + c[2]*position5[i];
            position3[i] = position3[i] + c[0]*position4[i] + c[1]*position5[i];
            position4[i] = position4[i] + c[0]*position5[i];
            position5[i] += 0.;

            // Is this line below necessary? Seems unnecessary to me.
            accelerationPredict[i] = accelerationTemp[i];
        }

        chainParticles->SetPosition(positionTemp);
        chainParticles->SetVelocity(velocityTemp);
        chainParticles->SetAccel(accelerationTemp);

        // Calculate accelerations for corrector step
        chainPotential[0].CalculateAcceleration(chainParticles);
        
        // Calculate external driving externalPerturbation
        chainParticles->GetAccel(accelerationPredict);
        double c1 = timeStep * timeStep / 2.0; // One of the 5 coefficients used by Predictor step of the algorithm.

        iExternalPerturbationManager->ApplyExternalPerturbationsAndCalculateUpdatedAccelerations(
            chainParticles,
            tCurrent,
            accelerationPredict);

        for (i = 0; i < systemSize; i++)
        {
            deltaR[i] = c1 * (accelerationPredict[i] - accelerationTemp[i]);
        }

        // Run corrector steps using information about calculated acceleration
        for (i = 0; i < systemSize; i++)
        {
            positionTemp[i] += a[0]*deltaR[i];
            velocityTemp[i] += a[1]*deltaR[i]*cInverse[0];
            accelerationTemp[i] += a[2]*deltaR[i]*cInverse[1];
            position3[i] += a[3]*deltaR[i]*cInverse[2];
            position4[i] += a[4]*deltaR[i]*cInverse[3];
            position5[i] += a[5]*deltaR[i]*cInverse[4];
        }

        // Again set values of dynamical variables
        chainParticles->SetPosition(positionTemp);
        chainParticles->SetVelocity(velocityTemp);
        chainParticles->SetAccel(accelerationTemp);

        int counter = n1/samplingFrequency; // variable to decide when to write to file

        if (counter == 1)
        {
            // 05/22/2022 RKP: Why do I need to calculate these values if I am
            // not writing them to file yet?
            printf("complete: %3f%%\n", n*100./totalTime);
            totalKineticEnergy = 0.;
            totalPotentialEnergy = 0.;
            totalEnergy = 0.;

            chainParticles->CalculateKE();
            chainParticles->GetKe(kineticEnergy);

            chainParticles->GetTotalKineticEnergy(totalKineticEnergy);

            totalPotentialEnergy = chainPotential[0].TotalPotentialEnergy(chainParticles, potentialEnergy);
            totalEnergy = totalKineticEnergy + totalPotentialEnergy; // calculate total energy
            if (currentCacheSize == maxCacheSize)
            {
                currentCacheSize = 0;
                WriteToFile();
                ClearVariableCache();
            }
            else
            {
                currentCacheSize++;
                SaveToCache(
                    positionTemp,
                    velocityTemp,
                    accelerationTemp,
                    kineticEnergy,
                    potentialEnergy,
                    mass,
                    totalEnergy
                );                
            }

            // n is the counter for recorded steps, n1 is the counter to check when to write
            n++;
            n1 = 0;
        }
        n1++;
    }

    // If any values in the variable cache have not been written then write them to file
    if (currentCacheSize > 0)
    {
        WriteToFile();
    }

    // After the simulation is done, write restart step
    std::vector<double> position;
    chainParticles->GetPosition(position);

    std::vector<double> velocity;
    chainParticles->GetVelocity(velocity);

    std::vector<double> acceleration;
    chainParticles->GetAccel(acceleration);

    WriteRestartFiles(position, velocity, acceleration);
}

template <class PotType>
void Simulation::f_velocityVerlet(std::shared_ptr<ChainParticles>& chainParticles, std::vector<PotType> &chainPotential)
{
    cout << "Velocity Verlet" << endl;

    // Initialize variables needed for the velocity Verlet algorithm

    // Temporary variables used to update the position, velocity and acceleration variables
    // 05/22/2022 RKP: Leaving these in because these are gcc specific and I may need to revive it
    //double positionTemp [systemSize]; //__attribute__((aligned));
    //double velocityTemp [systemSize]; //__attribute__((aligned));
    //double accelTemp [systemSize]; //__attribute__((aligned));
    std::vector<double> positionTemp;
    std::vector<double> velocityTemp;
    std::vector<double> accelTemp;
    
    // Initialize temporary variables
    unsigned int i;
    std::vector<double> mass;
    std::vector<double> kineticEnergy;
    std::vector<double> potentialEnergy(systemSize + 1, 0.);
    //double velocityTemp[systemSize]; // __attribute__((aligned));
    //double accelerationTemp[systemSize]; // __attribute__((aligned));
    //double externalPerturbationValues[systemSize]; // __attribute__((aligned));

    // Initialize temporary variables
    chainParticles->CalculateKE();
    chainParticles->GetPosition(positionTemp);
    chainParticles->GetVelocity(velocityTemp);
    chainParticles->GetAccel(accelTemp);
    chainParticles->GetMass(mass);
    chainParticles->GetKe(kineticEnergy);

    // Initialize variables for energies to be calculated and written to file

    double totalKineticEnergy = 0.;

    // Write initial data (at t = 0) to file
    std::shared_ptr<Output> outputFileWriter = Output::CreateOrGetSingleton(); // Write initial data (at t = 0) to file
    chainParticles->CalculateKE();
    chainParticles->GetKe(kineticEnergy);

    bool useOldPath = true;

    chainParticles->GetTotalKineticEnergy(totalKineticEnergy);

    double totalPotentialEnergy = 0.;
    totalPotentialEnergy = chainPotential[0].TotalPotentialEnergy(chainParticles, potentialEnergy);

    double totalEnergy = 0.;
    totalEnergy = totalKineticEnergy + totalPotentialEnergy; // calculate total energy

    SaveToCache(positionTemp,
        velocityTemp,
        accelTemp,
        kineticEnergy,
        potentialEnergy,
        mass,
        totalEnergy);
    
    // Initialize counter variables to decide when to write to file
    int counter;
    int n = 1; // Counter for total time
    int n1 = 1; // Counter for data write times
    double time = 0.;

    // Now start simulation loop
    while (n < totalTime)
    {
        time += timeStep;

        // Positions and half-way velocities calculated first
        for (int i = 0; i < systemSize; i++)
        {
            positionTemp[i] += timeStep * velocityTemp[i] + 0.5 * timeStep * timeStep * accelTemp[i];
        }

        for (i = 0; i < systemSize; i++)
        {
            velocityTemp[i] += 0.5*timeStep*accelTemp[i];
        }
        
        chainParticles->SetPosition(positionTemp);
        chainParticles->SetVelocity(velocityTemp);

        // Calculate accelerations
        chainPotential[0].CalculateAcceleration(chainParticles);

        chainParticles->GetAccel(accelTemp);

        // Calculate full step velocity
        for (i = 0; i < systemSize; i++)
        {
            velocityTemp[i] += 0.5*timeStep*accelTemp[i];
        }

        chainParticles->SetVelocity(velocityTemp);

        counter = n1/samplingFrequency;

        if (counter == 1)
        {
            // 05/22/2022 RKP: We don't need to calculate these values
            // if we are not writing
            printf("complete: %3f%%\n", n*100./totalTime);
            totalKineticEnergy = 0.;
            totalPotentialEnergy = 0.;
            totalEnergy = 0.;

            chainParticles->CalculateKE();
            chainParticles->GetKe(kineticEnergy);

            chainParticles->GetTotalKineticEnergy(totalKineticEnergy);

            totalPotentialEnergy = chainPotential[0].TotalPotentialEnergy(chainParticles, potentialEnergy);
            totalEnergy = totalKineticEnergy + totalPotentialEnergy; // calculate total energu

            if (currentCacheSize == maxCacheSize)
            {
                currentCacheSize = 0;
                WriteToFile();
                ClearVariableCache();
            }
            else
            {
                currentCacheSize++;
                SaveToCache(
                    positionTemp,
                    velocityTemp,
                    accelTemp,
                    kineticEnergy,
                    potentialEnergy,
                    mass,
                    totalEnergy
                );
            }

            // n is the counter for recorded steps, n1 is the counter to check when to write
            n++;
            n1 = 0;
        }
        n1++;
    }

    // If any values in the variable cache have not been written then write them to file
    if (currentCacheSize > 0)
    {
        WriteToFile();
    }

    // After the simulation is done, write restart step
    std::vector<double> position;
    chainParticles->GetPosition(position);

    std::vector<double> velocity;
    chainParticles->GetVelocity(velocity);

    std::vector<double> acceleration;
    chainParticles->GetAccel(acceleration);

    for (int ii = 0; ii < systemSize; ii++)
    {
        outputFileWriter->writeToResFile(position[ii], false);
        outputFileWriter->writeToResFile(velocity[ii], false);
        outputFileWriter->writeToResFile(acceleration[ii], true);
    }

    std::cout << "total time = " << time << std::endl;
}

#endif // SIMULATION_H
