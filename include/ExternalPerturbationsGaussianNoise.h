#pragma once
#include "../include/ExternalPerturbationBase.h"
#include <string>
#include <random>

class ChainParticles;

class ExternalPerturbationsGaussianNoise : public ExternalPerturbation
{
public:
	ExternalPerturbationsGaussianNoise(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

private:
	std::string mPertubationFunction;
	double mDrivingAmplitude;
	double mMu;
	double mSigma;
	double mStartTime;
	double mEndTime;

	std::normal_distribution<double> mDistribution;
	std::mt19937 mGenerator;

	void CalculateRandomPerturbation(double& oPerturbationValue);

	// Inherited via ExternalPerturbation
	virtual void vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
		std::shared_ptr<ChainParticles> ioChainParticles, 
		double iCurrentTime, 
		std::vector<double>& oPredictedAcceleration) override;

	virtual void vSetParametersForExternalPerturbation(
		std::string iType, 
		std::vector<double> iParameters) override;		
};