#include "../include/ExternalPerturbationBase.h"

class ChainParticles;

class ExternalPerturbationsSawtooth : public ExternalPerturbation
{
public:
	ExternalPerturbationsSawtooth(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

private:
	std::string mPerturbationFunction;
	double mWaveStartValue;
	double mWaveEndValue;
	double mWidth;
	double mStartTime;
	double mEndTime;

	double mSlopeOfSawToothWave;

	// Ideally this should be linked to the step size but still needs to be hooked in somehow.
	double mTolerance = 0.000001;

	void CalculateSlopeOfSawToothWave();

	void CalculateSawToothWavePerturbation(
		double& oPerturbationValue,
		const double& iCurrentTime);

	// Inherited via ExternalPerturbation
	virtual void vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
		std::shared_ptr<ChainParticles> ioChainParticles, 
		double iCurrentTime, 
		std::vector<double>& oPredictedAcceleration) override;

	virtual void vSetParametersForExternalPerturbation(
		std::string iType, 
		std::vector<double> iParameters) override;

};