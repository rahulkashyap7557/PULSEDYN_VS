#pragma once
#include "../include/ExternalPerturbationBase.h"

class ChainParticles;

class ExternalPerturbationsVelocityDamping : public ExternalPerturbation
{
public:
	ExternalPerturbationsVelocityDamping(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize
	);

private:

	double mGamma = 0.;
	std::string mPerturbationVariable;

	// Inherited via ExternalPerturbation
	virtual void vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
		std::shared_ptr<ChainParticles> ioChainParticles, 
		double iCurrentTime, 
		std::vector<double>& oPredictedAcceleration) override;

	virtual void vSetParametersForExternalPerturbation(
		std::string iType, 
		std::vector<double> iParameters) override;
};