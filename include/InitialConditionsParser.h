#ifndef INITIALCONDITIONSPARSER_H
#define INITIALCONDITIONSPARSER_H
#include "OutputFileNames.h"
#include "allIncludes.h"
#include <map>
#include "ExternalPerturbationsManager.h"

class ChainParticles;

class InitialConditionsParser
{
public:
    static std::shared_ptr<InitialConditionsParser> Create();
    bool ParseParameterFile();
    void SetVariables(Simulation& oSimParams,
        std::shared_ptr< ChainParticles>& oChainParticles);
    std::string GetSpringType();
    bool CheckAndDeleteExistingOutputFiles();

    double GetK1();
    double GetK2();
    double GetK3();

    std::shared_ptr<ExternalPerturbationsManager> GetExternalPerturbationsManager()
    {
        return this->mExternalPerturbations;
    }

private:
    // Stores the names of the output files identified by their enums
    std::map<OUTPUT_FILE_NAMES, std::string> outputFileNameMap;

    std::shared_ptr<ExternalPerturbationsManager> mExternalPerturbations;

    unsigned int systemSize = 100;
    double dt = 0.01;
    int samplingFrequency = 1 / dt;
    double pi = 3.14159;
    int totalTime = 100;
    std::string method = "gear5";
    int sampleFlag = 0;

    // Initialize model parameters - default FPUT model
    double k1 = 0.2;
    double k2 = 0.0;
    double k3 = 1.0;

    std::vector<double> mass;
    double defaultMass = 1.0;
    string springType = "fput";

    // Initialize boundary conditions
    string lboundary = "periodic";
    string rboundary = "periodic";

    std::vector<unsigned int> perturbedParticles;
    std::vector<string> perturbationType;
    std::vector<double> perturbationAmplitude;
    std::vector<unsigned int> impurityParticles;
    std::vector<double> impurityValue;

    InitialConditionsParser();

    // Should be moved out of here eventually into a class for output files
    void BuildOutputFileMapper();

};

#endif // INITIALCONDITIONSPARSER_H
