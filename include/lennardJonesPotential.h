/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef LENNARDJONESPOTENTIAL_H
#define LENNARDJONESPOTENTIAL_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "Particle.h"
#include "boundaryConditions.h"

class lennardJonesPotential
{
    public:
        /** Default constructor */
        lennardJonesPotential();
        lennardJonesPotential(Particle chainParticles);

        double Getk1() { return k1; }
        /** Set mass
         * \param val New value to set
         */
        void Setk1(double val) { k1 = val; }
        /** Access position
         * \return The current value of position
         */

        double Getk2() { return k2; }
        /** Set mass
         * \param val New value to set
         */
        void Setk2(double val) { k2 = val; }
        /** Access position
         * \return The current value of position
         */

        void CalculateAcceleration(std::shared_ptr<ChainParticles>& iChainParticles)
        {
            string lb, rb;

            unsigned int systemSize = 0;
            iChainParticles->GetSystemSize(systemSize);
            iChainParticles->GetLeftBoundary(lb);
            iChainParticles->GetRightBoundary(rb);
            unsigned int systemSizeMinus1 = systemSize - 1;

            std::vector<double> position;
            iChainParticles->GetPosition(position);

            std::vector<double> massVector;
            iChainParticles->GetMass(massVector);

            std::vector<double> accel(systemSize, 0.0);

            double k1p;
            k1p = pow(k1, 6);

            // Calculate the non boundry particles first and then figure out the boundaries
            for (int ii = 1; ii < systemSizeMinus1; ii++)
            {
                double fac1;
                double fac2;
                int k = ii + 1;
                int j = ii - 1;
                fac2 = position[ii] - position[k];
                fac1 = position[ii] - position[j];

                double Dfac1, Dfac2;
                Dfac1 = fac1 + k1;
                Dfac2 = k1 - fac2;
                double term1;
                term1 = pow(Dfac1, -7) - k1p * pow(Dfac1, -13);
                double term2;
                term2 = pow(Dfac2, -7) - k1p * pow(Dfac2, -13);

                double a = 0.;
                a = term2 - term1;
                a = 12.0 * k2 * k1p * a;
                a = a / massVector[ii];
                accel[ii] = a;
            }

            // Now calculate accceleration for left boundary;
            double fac1 = f_leftBoundaryConditionsAccel(iChainParticles);
            double fac2 = position[0] - position[1];
            double a;

            double Dfac1, Dfac2;
            Dfac1 = fac1 + k1;
            Dfac2 = k1 - fac2;
            double term1;
            term1 = pow(Dfac1, -7) - k1p * pow(Dfac1, -13);
            double term2;
            term2 = pow(Dfac2, -7) - k1p * pow(Dfac2, -13);

            a = 0.;
            a = term2 - term1;
            a = 12.0 * k2 * k1p * a;
            a = a / massVector[0];
            accel[0] = a;

            // Next calculate the acceleration for right most boundary
            fac1 = position[systemSizeMinus1] - position[systemSizeMinus1 - 1];
            fac2 = f_rightBoundaryConditionsAccel(iChainParticles);

            Dfac1 = fac1 + k1;
            Dfac2 = k1 - fac2;
            term1 = pow(Dfac1, -7) - k1p * pow(Dfac1, -13);
            term2 = pow(Dfac2, -7) - k1p * pow(Dfac2, -13);

            a = 0.;
            a = term2 - term1;
            a = 12.0 * k2 * k1p * a;
            a = a / massVector[systemSizeMinus1];
            accel[systemSizeMinus1] = a;

            // Set acceleration back in the chainParticles object
            iChainParticles->SetAccel(accel);

            //lb = chainParticles.at(0).Getlboundary();
            //rb = chainParticles.at(N).Getrboundary();
        }

        double TotalPotentialEnergy(std::shared_ptr<ChainParticles>& iChainParticles,
            std::vector<double>& oPotentialEnergy)
        {
            std::vector<double> position;
            iChainParticles->GetPosition(position);
            unsigned int systemSize;
            iChainParticles->GetSystemSize(systemSize);
            unsigned int systemSizeMinus1 = systemSize - 1;

            double totalPotentialEnergy = 0.0;
            //std::vector<double> potentialEnergy(systemSize + 1, 0.0);
            // Do the inner particles and then do the boundaries separately
            for (unsigned int ii = 1; ii < systemSize; ii++)
            {
                unsigned int jj = ii - 1;
                double dx = position[ii] - position[jj];
                //double fac = dx * dx;
                double term1 = 0.0;
                double term2 = 0.0;
                term1 = pow(k1, 12) * pow(k1 + dx, -12);
                term2 = 2. * pow(k1, 6) * pow(k1 + dx, -6);
                oPotentialEnergy[ii] = k2 * (term1 - term2 + 1.);
            }

            // Now add the potential energy of the first spring
            double dxLeftBoundary = f_leftBoundaryConditionsPe(iChainParticles);
            
            double term1 = pow(k1, 12) * pow(k1 + dxLeftBoundary, -12);
            double term2 = 2. * pow(k1, 6) * pow(k1 + dxLeftBoundary, -6);

            oPotentialEnergy[0] = k2 * (term1 - term2 + 1.);

            // Add potential energy of the right most spring
            double dxRightBoundary = f_rightBoundaryConditionsPe(iChainParticles);

            term1 = pow(k1, 12) * pow(k1 + dxRightBoundary, -12);
            term2 = 2. * pow(k1, 6) * pow(k1 + dxRightBoundary, -6);
            oPotentialEnergy[systemSize] = k2 * (term1 - term2 + 1.);


            for (int ii = 0; ii < oPotentialEnergy.size(); ii++)
            {
                totalPotentialEnergy += oPotentialEnergy[ii];
            }

            return totalPotentialEnergy;
        }


    protected:

    private:
        double k1;
        double k2;
};

#endif // LENNARDJONESPOTENTIAL_H


