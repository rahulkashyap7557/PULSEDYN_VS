#pragma once
#include <vector>
#include <string>
#include <memory>

class ExternalPerturbation;
class ChainParticles;

class ExternalPerturbationsManager
{
public:

	static std::shared_ptr<ExternalPerturbationsManager> Create();

	void AppendExternalPerturbation(std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber,
		std::vector<double> iParameters,
		unsigned int iSystemSize);

	void ApplyExternalPerturbationsAndCalculateUpdatedAccelerations(
		std::shared_ptr<ChainParticles> ioChainParticles,
		double iCurrentTime,
		std::vector<double>& oAccelerationPredicted);

private:
	ExternalPerturbationsManager();
	std::vector<std::shared_ptr<ExternalPerturbation>> externalPerturbations;

};
