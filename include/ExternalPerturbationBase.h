#pragma once
#include <string>
#include <vector>
#include <memory>

class ChainParticles;

class ExternalPerturbation
{
public:
	static std::shared_ptr<ExternalPerturbation>CreateExternalPerturbation(
		std::string iType,
		std::string iExternalPerturbationVariable,
		std::string iParticleNumber, 
		std::vector<double> iParameters,
		unsigned int iSystemSize);

	void ApplyExternalPerturbationAndCalculateUpdatedAcceleration(
		std::shared_ptr<ChainParticles> ioChainParticles,
		double iCurrentTime,
		std::vector<double>& oPredictedAcceleration
	);
	
protected:

	std::vector<double> mParticlesToApplyForceOn;
	std::string mExternalPerturbationVariable = "notset";

	std::vector<unsigned int> mParticlesToApplyExternalPerturbationOn;

	void SetParticlesToApplyPerturbationOn(
		std::string& iParticleNumber,
		unsigned int iSystemSize);

	void SetExternalPerturbationVariable(std::string iExternalPerturbationVariable);
	
	void SetParametersForExternalPerturbation(
		std::string iType,
		std::vector<double> iParameters);

private:
	virtual void vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
		std::shared_ptr<ChainParticles> ioChainParticles,
		double iCurrentTime,
		std::vector<double>& oPredictedAcceleration
	) = 0;

	virtual void vSetParametersForExternalPerturbation(
		std::string iType,
		std::vector<double> iParameters) = 0;
};