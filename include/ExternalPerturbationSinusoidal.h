/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef ExternalPerturbation_H
#define ExternalPerturbation_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "Particle.h"
#include "ExternalPerturbationBase.h"

using namespace std;

class ExternalPerturbationSinusoidal : public ExternalPerturbation
{
    public:
        /** Default constructor */
        ExternalPerturbationSinusoidal();

        ExternalPerturbationSinusoidal(
            std::string iType,
            std::string iExternalPerturbationVariable,
            std::string iParticleNumber,
            std::vector<double> iParameters,
            unsigned int iSystemSize
        );

        // ExternalPerturbation functions

        double f_forceCalc(double tCurrent);

        double f_cosine(double tCurrent);
        double f_sine(double tCurrent);
        double f_constant(double tCurrent);

    protected:

    private:
        // perturbation function can be sine, cosine or constant
        string perturbationFunction; //!< Member variable "type"
        string perturbationVariable;
        double t1; //!< Member variable "t1"
        double t2; //!< Member variable "t2"
        double t3; //!< Member variable "t3"
        double frequency; //!< Member variable "frequency"
        double ramp; //!< Member variable "ramp"
        double amp; //!< Member variable "amp"
        double gamma;

        // Inherited via ExternalPerturbation
        virtual void vApplyExternalPerturbationAndCalculateUpdatedAcceleration(
            std::shared_ptr<ChainParticles> ioChainParticles, 
            double iCurrentTime, std::vector<double>& oPredictedAcceleration) override;

        // Inherited via ExternalPerturbation
        virtual void vSetParametersForExternalPerturbation(
            std::string iType, 
            std::vector<double> iParameters) override;
};

#endif // ExternalPerturbation_H
