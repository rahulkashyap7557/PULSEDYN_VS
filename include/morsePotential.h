/*
    Copyright (c) Rahul Kashyap 2017

    This file is part of PULSEDYN.

    PULSEDYN is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    PULSEDYN is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with PULSEDYN.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef MORSEPOTENTIAL_H
#define MORSEPOTENTIAL_H
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include "Particle.h"
#include "boundaryConditions.h"

class morsePotential
{
    public:
        /** Default constructor */
        morsePotential();
        morsePotential(Particle chainParticles);

        double Getk1() { return k1; }
        /** Set mass
         * \param val New value to set
         */
        void Setk1(double val) { k1 = val; }
        /** Access position
         * \return The current value of position
         */

        double Getk2() { return k2; }
        /** Set mass
         * \param val New value to set
         */
        void Setk2(double val) { k2 = val; }
        /** Access position
         * \return The current value of position
         */

        void CalculateAcceleration(std::shared_ptr<ChainParticles>& iChainParticles)
        {
            string lb, rb;

            unsigned int systemSize = 0;
            iChainParticles->GetSystemSize(systemSize);
            iChainParticles->GetLeftBoundary(lb);
            iChainParticles->GetRightBoundary(rb);
            unsigned int systemSizeMinus1 = systemSize - 1;

            std::vector<double> position;
            iChainParticles->GetPosition(position);

            std::vector<double> massVector;
            iChainParticles->GetMass(massVector);

            std::vector<double> accel(systemSize, 0.0);
            // Calculate the non boundry particles first and then figure out the boundaries

            for (int ii = 1; ii < systemSizeMinus1; ii++)
            {
                double fac1;
                double fac2;
                int k = ii + 1;
                int j = ii - 1;
                fac2 = position[ii] - position[k];
                fac1 = position[ii] - position[j];

                double a;
                a = 2. * k2 * k1 * (exp(k2 * fac2) - exp(2. * k2 * fac2) + exp(-2. * k2 * fac1) - exp(-k2 * fac1));
                a = a / massVector[ii];
                accel[ii] = a;
            }

            // Now calculate accceleration for left boundary;
            double fac1 = f_leftBoundaryConditionsAccel(iChainParticles);
            double fac2 = position[0] - position[1];
            
            double a;
            a = 2. * k2 * k1 * (exp(k2 * fac2) - exp(2. * k2 * fac2) + exp(-2. * k2 * fac1) - exp(-k2 * fac1));
            a = a / massVector[0];
            accel[0] = a;

            // Next calculate the acceleration for right most boundary
            fac1 = position[systemSizeMinus1] - position[systemSizeMinus1 - 1];
            fac2 = f_rightBoundaryConditionsAccel(iChainParticles);

            a = 2. * k2 * k1 * (exp(k2 * fac2) - exp(2. * k2 * fac2) + exp(-2. * k2 * fac1) - exp(-k2 * fac1));
            a = a / massVector[systemSizeMinus1];
            accel[systemSizeMinus1] = a;

            // Set acceleration back in the chainParticles object
            iChainParticles->SetAccel(accel);
        }

        double TotalPotentialEnergy(std::shared_ptr<ChainParticles>& iChainParticles,
            std::vector<double>& oPotentialEnergy)
        {
            std::vector<double> position;
            iChainParticles->GetPosition(position);
            unsigned int systemSize;
            iChainParticles->GetSystemSize(systemSize);
            unsigned int systemSizeMinus1 = systemSize - 1;

            double totalPotentialEnergy = 0.0;
            // Do the inner particles and then do the boundaries separately
            for (unsigned int ii = 1; ii < systemSize; ii++)
            {
                unsigned int jj = ii - 1;
                double dx = position[ii] - position[jj];
                double fac = dx * dx;

                oPotentialEnergy[ii] = k1 * (exp(-k2 * dx) - 1) * (exp(-k2 * dx) - 1);
            }

            // Now add the potential energy of the first spring
            double dxLeftBoundary = f_leftBoundaryConditionsPe(iChainParticles);
            oPotentialEnergy[0] = k1 * (exp(-k2 * dxLeftBoundary) - 1) * (exp(-k2 * dxLeftBoundary) - 1);

            // Add potential energy of the right most spring
            double dxRightBoundary = -1.0*f_rightBoundaryConditionsPe(iChainParticles);
            oPotentialEnergy[systemSize] = k1 * (exp(-k2 * dxRightBoundary) - 1) * (exp(-k2 * dxRightBoundary) - 1);

            for (int ii = 0; ii < oPotentialEnergy.size(); ii++)
            {
                totalPotentialEnergy += oPotentialEnergy[ii];
            }

            return totalPotentialEnergy;
        }

    protected:

    private:
        double k1;
        double k2;
};

#endif // morsePOTENTIAL_H

