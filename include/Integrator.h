#pragma once
#include <memory>
#include <string>

class Integrator
{
public:
	static std::shared_ptr<Integrator> Create(std::string iTypeOfIntegrator);
	double GetTimeStep();
	void SetTimeStep(double iTimeStep);

private:
	double mTimeStep;
};